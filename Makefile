# Copyright 2000 Pace Micro Technology plc
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Makefile for AREncode
#

COMPONENT       = AREncode
override TARGET = !RunImage
CDEFINES        = -DNDEBUG
CDFLAGS         = -DTRACE=1
OBJS            = main compress cvtwin hdrwin calldiffer prefs tracks filters
LIBS            = ${ARLIBS}
INSTTYPE        = app
INSTAPP_FILES   = !Boot !Help !Run !RunImage Messages Templates \
                  !Sprites:Themes !Sprites11:Themes !Sprites22:Themes \
                  Morris4.!Sprites:Themes.Morris4 \
                  Ursula.!Sprites:Themes.Ursula
INSTAPP_VERSION = Messages

include CApp

# Dynamic dependencies:
